//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#include <vector>
#include <cmath>
#include <fstream>
#include <numeric>
#include <memory>
#include <iostream>
#include "Attractor.h"
#include "Routine.h"
#include "Network.h"
#include "Update.h"
#include "TransferFunction.h"

// _____________________________________________________________________
// Constructor for the attractor class.
Attractor::Attractor()
{

}
// _____________________________________________________________________
// BinToBip convert binary activity to a bipolar activity
std::vector<int> Attractor::BinToBip(std::vector<int> & Activity)
{
    Network Net;

    for(int i = 0; i < Net._AdSize; ++i)
    {
        Activity[i]=2*Activity[i]-1;
    }

    return Activity;
}
// _____________________________________________________________________
// BipToBin convert bipolar activity to a binary activity
std::vector<int> Attractor::BipToBin(std::vector<int> & Activity)
{
    Network Net;

    for(int i = 0; i < Net._AdSize; ++i)
    {
        Activity[i] = (Activity[i]+1)/2;
    }

    return Activity;
}
// _____________________________________________________________________
// ActivityToNumber convert an binary activity to a number
int Attractor::ActivityToNumber(std::vector<int> & Activity)
{
    Network Net;
    int number;
    number=0;
    for(int j=0; j<Net._AdSize; ++j)
    {
        number=number + Activity[j]*std::pow(2,j);
    }
    return number;
}
// _____________________________________________________________________
// NumberToActivity convert a number to a binary activity
// int2bits_vector could be a good name too
std::vector<int> Attractor::NumberToActivity(int & number)
{
    Network Net;
    std::vector<int> activity(Net._AdSize);

    for(auto &i : activity)
    {
        i = number % 2;
        number /= 2;
    }

    return activity;
}
// _____________________________________________________________________
// Analysis for the attractor landscape in binary encoding
std::vector<std::vector<int> > Attractor::AnalysisBinary(std::vector< std::vector<int> > & Matrix)
{
    std::ofstream print("./Output.txt",std::ios_base::app);
    std::ofstream print2Rbin_SizeOfBasins("./SizeOfBasinsBin.txt",std::ios_base::out);
    std::ofstream print2Rbin_DotProductAttractorsAdjointCSpace("./DotProductAttractorsAdjointCSpaceBin.txt",std::ios_base::out);
    std::ofstream print2Rbin_HammingDistanceAttractorsAdjointSpace("./HammingDistanceAttractorsAdjointSpaceBin.txt",std::ios_base::out);
    std::ofstream printLog("./log.txt",std::ios_base::app);

// Declare attractor analysis
    Network Net;
    Network Var;
    TransferFunction Theta;

    std::vector<int> basins(Var._SampleSize);

    std::vector< std::vector<int> > attractor(4000, std::vector<int>(52));
    std::vector<int> neurons(Net._AdSize);
    std::vector<int> helpneurons(Net._AdSize);

    int ncounter;
    int nattractor;
    int ntest;
    int ih1, ih2, ih3, i_att;
    int nNumber;
    int i, j, k;

// Initialize attractor counter
    for(i=0; i<Var._SampleSize; ++i)
    {
        basins[i]=0;
    }

    for(i=0; i<4000; ++i)
    {
        for(j=0; j<52; ++j)
        {
            attractor[i][j]=0;
        }
    }

    neurons = {0};
    ncounter=0;
    nattractor=1;
    basins[0]=1;

// Investigate attractors
    while(ncounter<Var._SampleSize)
    {
        helpneurons = {0};
        helpneurons = Update::Hebb(Matrix, neurons); // Update
        neurons = TransferFunction::StepFunctionBin(helpneurons); //Activation function

        nNumber=0;
        nNumber=ActivityToNumber(neurons); // Neural activity to number

        if(basins[nNumber]==0)
        {
            basins[nNumber]=nattractor;
        }

        else
        {
            if(basins[nNumber]==nattractor)
            {

                if(nattractor>4000)
                {
                    print << "Warning: attractor > 4000";
                    exit(EXIT_FAILURE);
                }

                ++nattractor;
                while ( ( ncounter < Var._SampleSize ) && ( basins[ncounter] > 0 ) )
                {
                    ++ncounter;
                }
                if ( ncounter == Var._SampleSize )
                {
                    break;
                }

                basins[ncounter]=nattractor;
                neurons=NumberToActivity(ncounter); // Number to neural activity
            }

            else
            {
                for(i=0; i<Var._SampleSize; ++i)
                {
                    if(basins[i]==nattractor)
                    {
                        basins[i]=basins[nNumber];
                    }
                }

                while ( ( ncounter < Var._SampleSize ) && ( basins[ncounter] > 0 ) )
                {
                    ++ncounter;
                }
                if ( ncounter == Var._SampleSize )
                {
                    break;
                }

                basins[ncounter]=nattractor;
                neurons=NumberToActivity(ncounter);
            }
        }
    }

    for(i=0; i<Var._SampleSize; ++i)
    {
        attractor[basins[i]][50]=attractor[basins[i]][50]+1;
    }

    for(i_att=1; i_att<nattractor; ++i_att)
    {
        ih1=0;

        while(basins[ih1]!=i_att)
        {
            ih1++;
        }

        neurons=NumberToActivity(ih1);

//Update neural activity 70 times
        for(k=0; k<70; ++k)
        {
            helpneurons = {0};
            helpneurons = Update::Hebb(Matrix, neurons); // Update
            neurons = TransferFunction::StepFunctionBin(helpneurons); // Activation function
        }

        nNumber=0;
        nNumber=ActivityToNumber(neurons); // Neural activity to number

        ntest=0;
        ih2=1;
        ih3=nNumber;

        while(nNumber!=ih2)
        {
            ntest++;
            attractor[i_att][ntest]=ih3;

            helpneurons = {0};
            helpneurons = Update::Hebb(Matrix, neurons); // Update
            neurons = TransferFunction::StepFunctionBin(helpneurons); // Activation function

            ih3=0;
            ih3=ActivityToNumber(neurons); // Neural activity to number
            ih2=ih3;
        }

        attractor[i_att][51]=ntest;
        if(ntest>50)
        {
            printLog << "Warning: size of attractor > 50";
            exit(EXIT_FAILURE);
        }
    }

// Print first results of the attractor analysis
    print << "\nResults of the attractor analysis:" << "\n";
    print << "\nNumber of basins: " << (nattractor-1) << "\n";

    for(i=1; i<nattractor; ++i)
    {
        print << "\nAttractor: " << i;
        print << "\nSize of the basin: " << attractor[i][50];
        print << "\nNumber of states: " << attractor[i][51] << "\n";

        for(j=1; j<(attractor[i][51]+1); ++j)
        {
            //print  << "Binary exponentiation: " << attractor[i][j];
            print  << "\nNeural Activity: ";
            ih1=attractor[i][j];
            neurons=NumberToActivity(ih1); // Number to neural activity
            for(k=0; k<Net._AdSize; ++k)
            {
                print << neurons[k] << " ";
            }
            print << "\n";
        }
    }

// Print the size of the basins
    print2Rbin_SizeOfBasins << "#Size of the basins" << "\n";
    for(i=1; i<nattractor; ++i)
    {
        print2Rbin_SizeOfBasins << attractor[i][50] << "\n";
    }

// Store attractors in a matrix
    std::vector< std::vector<int> > Attractors(nattractor-1, std::vector<int>(Net._AdSize));

    for(i=1; i<nattractor; ++i)
    {
        for(j=1; j<(attractor[i][51]+1); ++j)
        {
            ih1=attractor[i][j];
            for(k=0; k<Net._AdSize; ++k)
            {
                Attractors[i-1][k]=ih1%2;
                ih1=(ih1-Attractors[i-1][k])/2;
            }
        }
    }

// Initialize configuration space
    std::vector< std::vector<int> > CSpace(Var._SampleSize, std::vector<int>(Net._AdSize +1));
    int ipattern;
    for(i=0; i<Var._SampleSize; i++)
    {
        for(j=0; j<Net._AdSize+1; j++)
        {
            CSpace[i][j] = 0;
        }
    }

// Set configuration space
    for(ipattern=0; ipattern<Var._SampleSize; ++ipattern)
    {
        j=ipattern;
        k=0;
        for (i=1; i<Net._AdSize; i++)
        {
            CSpace[ipattern][i+1]=0;
        }
        while(j>0) // Set Pattern from ipattern
        {
            CSpace[ipattern][k+1]=j%2;
            j=(j-j%2)/2;
            k=k+1;
        }
    }

// Append attractor counter to the configuration space. First Column correspond to the attractor
    std::vector<int> AttrCount(Var._SampleSize);
    for(i=0; i<Var._SampleSize; i++)
    {
        AttrCount[i]=basins[i];
    }

    for(i=0; i<Var._SampleSize; ++i)
    {
        int AttrIndex = AttrCount[i];
        CSpace[i][0]=AttrIndex;
    }

// Print configurationspace.
//print << "\n#Attractor-No.  State vector" << "\n";
//for(const std::vector<int> &i: CSpace){
//  for(int j: i){print << j << ' ';}
//print << "\n";
//}

// Calculate inner products < attractors | adjoint state vectors>
// Initialize matrix DotProductAttractorsAdjointCSpace
    std::vector< std::vector<int> > DotProductAttractorsAdjointCSpace(nattractor-1, std::vector<int>(Var._SampleSize));
    std::vector< std::vector<int> > HammingDistanceAttractorsAdjointSpace(nattractor-1, std::vector<int>(Var._SampleSize));
    std::vector<int> vectorA(Net._AdSize);
    std::vector<int> vectorB(Net._AdSize);
    std::vector<int> index(nattractor-1);

    for( int y = 0; y < Var._SampleSize; y++)
    {
        for(i=0; i<Net._AdSize; ++i)
        {
            vectorA[i] = Attractors[CSpace[y][0]-1 ][i];
        }

        //std::cout << "A:  ";
        //for(auto i:vectorA){std::cout << i << " ";}
        //std::cout << std::endl;

        for( int x = 1; x < Net._AdSize+1; x++) //std::cout << CSpace[y][x] << "  ";
        {
            vectorB[x -1] = CSpace[y][x];
        }
        //std::cout << std::endl;

        //std::cout << "B:  ";
        //for(auto i:vectorB){std::cout << i << " ";}
        //std::cout << std::endl;

        HammingDistanceAttractorsAdjointSpace[ CSpace[y][0] -1][index[ CSpace[y][0] -1] ] = Routine::HammingDistance(vectorA, vectorB); // Calculate Hamming distance
        DotProductAttractorsAdjointCSpace[ CSpace[y][0] -1][index[ CSpace[y][0] -1] ] = inner_product(vectorA.begin(), vectorA.end(), vectorB.begin(), 0); // Calculate inner product
        index[CSpace[y][0]-1] += 1;
    }


// Print DotProductAttractorsAdjointCSpace
    print2Rbin_DotProductAttractorsAdjointCSpace << "#DotProductAttractorsAdjointCSpace" << "\n";
    for ( const std::vector<int> &i : DotProductAttractorsAdjointCSpace )
    {
        for ( int j : i )
        {
            print2Rbin_DotProductAttractorsAdjointCSpace << j << ' ';
        }
        print2Rbin_DotProductAttractorsAdjointCSpace << "\n";
    }


// Print HammingDistanceAttractorsAdjointSpace
    print2Rbin_HammingDistanceAttractorsAdjointSpace << "#HammingDistanceAttractorsAdjointSpace" << "\n";
    for ( const std::vector<int> &i : HammingDistanceAttractorsAdjointSpace )
    {
        for ( int j : i )
        {
            print2Rbin_HammingDistanceAttractorsAdjointSpace << j << ' ';
        }
        print2Rbin_HammingDistanceAttractorsAdjointSpace << "\n";
    }

    /*
    // _____________________________________________________________________
    // C-attractor output
    std::ofstream printCattractor("./CattractorOutput.txt",std::ios_base::app);
    printCattractor << "=====C-Attractor output==================Binary encoding=============" << std::endl;

    int neuronen_bin[Net._AdSize];
    int hilfsneuronen_bin[Net._AdSize];
    int hilfsbild[Net._AdSize];
    int ik, il, in, ip, ibild, itest, mtest, ncount;
    int ncorrectimage;               // counts correct recognized pattern

    // Algorithm
    ncorrectimage=0;
    for(ibild=0;ibild<Var._SampleSize;++ibild){

     il=ibild; in=0; for (ik=0;ik<Net._AdSize;++ik) {hilfsbild[ik]=0;} // Initialisierung des Testmusters

     while(il>0) // Bestimmung des Testmusters aus ibild
     {
     hilfsbild[in]=il%2;
     il=(il-il%2)/2;
     in=in+1;
     }

     printCattractor << "\nPattern " << ibild << ": ";
     for(ik=0;ik<Net._AdSize;++ik)
     {
     printCattractor << hilfsbild[ik] << " ";
     }

     printCattractor << "    ";
     for(ik=0;ik<Net._AdSize;++ik) {neuronen_bin[ik]=hilfsbild[ik];}

     itest=1;
     ntest=0;
     ncount=1;
     mtest=0;

     while (itest>0&&mtest<50) {
                  for (ik=0;ik<Net._AdSize;++ik) {
                      hilfsneuronen_bin[ik]=0;
                      for (il=0;il<Net._AdSize;++il) {
                          hilfsneuronen_bin[ik]=hilfsneuronen_bin[ik]+Matrix[ik][il]*neuronen_bin[il]; //Update Regel
                      }
                      if (hilfsneuronen_bin[ik]>=Theta._Threshold) { //Treshold
                          hilfsneuronen_bin[ik]=1;
                      }
                      else hilfsneuronen_bin[ik]=0;
                      in=(neuronen_bin[ik]-hilfsneuronen_bin[ik]);
                      ntest=ntest+in*in;
                  }

                  if(mtest>45){
                      printCattractor << " mtest: " << mtest << " Activity:  ";
                      for(ip=0;ip<Net._AdSize;++ip) {
                          printCattractor << neuronen_bin[ip] << " "; printCattractor << neuronen_bin[ip] << " ";}
                      printCattractor << "\n";
                  }

     itest=ntest;
     mtest=mtest+1;
     ntest=0;
     ncount=ncount+1;

     for (ik=0;ik<Net._AdSize;++ik) {neuronen_bin[ik]=hilfsneuronen_bin[ik];}
     }

     printCattractor << "   Attractor: "; //std::cout << "\nn= " << ncount;

     for (ik=0;ik<Net._AdSize;++ik)
     {
     printCattractor << neuronen_bin[ik] << " ";
     }
     il=0;

     for (ik=0;ik<Net._AdSize;++ik)
     {
     in=(neuronen_bin[ik]-hilfsbild[ik]);
     il=il+in*in;
     }

     if (il<1)
     {
     ncorrectimage=ncorrectimage+1;
     printCattractor << std::endl << "Attractor No.: " << ncorrectimage << std::endl;
     }

    }
    printCattractor << std::endl << "\nNumber of found attractors: " << ncorrectimage << std::endl;
    // _____________________________________________________________________
    */

    return Attractors;
}
// _____________________________________________________________________
// Analysis for the attractor landscape in bipolar encoding
std::vector<std::vector<int> > Attractor::AnalysisBipolar(std::vector< std::vector<int> > & Matrix)
{
    std::ofstream print("./Output.txt",std::ios_base::app);
    std::ofstream print2Rbip_SizeOfBasins("./SizeOfBasinsBip.txt",std::ios_base::out);
    std::ofstream print2Rbip_DotProductAttractorsAdjointCSpace("./DotProductAttractorsAdjointCSpaceBip.txt",std::ios_base::out);
    std::ofstream print2Rbip_HammingDistanceAttractorsAdjointSpace("./HammingDistanceAttractorsAdjointSpaceBip.txt",std::ios_base::out);
    std::ofstream printLog("./log.txt",std::ios_base::app);

// Declare attractor analysis
    Network Net;
    Network Var;
    TransferFunction Theta;

    std::vector<int> basins(Var._SampleSize);

    std::vector< std::vector<int> > attractor(4000, std::vector<int>(52));
    std::vector<int> neurons(Net._AdSize);
    std::vector<int> helpneurons(Net._AdSize);

    int ncounter;
    int nattractor;
    int ntest;
    int ih1, ih2, ih3, i_att;
    int nNumber;
    int i, j, k;

// Initialize attractor counter
    for(i=0; i<Var._SampleSize; ++i)
    {
        basins[i]=0;
    }

    for(i=0; i<4000; ++i)
    {
        for(j=0; j<52; ++j)
        {
            attractor[i][j]=0;
        }
    }

// Initialize neural activity
    for(i=0; i<Net._AdSize; i++)
    {
        neurons[i] = 0;
    }

    ncounter=0;
    nattractor=1;
    basins[0]=1;

// Investigate attractors
    while(ncounter<Var._SampleSize)
    {

        neurons=BinToBip(neurons); //std::cout << std::endl; for(i=0; i<Net._AdSize; i++){std::cout << neurons[i] << " ";} // NEU
        for(i=0; i<Net._AdSize; i++)
        {
            helpneurons[i] = 0;
        }
        helpneurons = Update::Hebb(Matrix, neurons); //std::cout << std::endl; for(i=0; i<Net._AdSize; i++){std::cout << neurons[i] << " ";} // Update
        neurons = TransferFunction::StepFunctionBip(helpneurons); //std::cout << std::endl; for(i=0; i<Net._AdSize; i++){std::cout << neurons[i] << " ";} //Activation function
        neurons=BipToBin(neurons); //std::cout << std::endl; for(i=0; i<Net._AdSize; i++){std::cout << neurons[i] << " ";} // NEU

        nNumber=0;
        nNumber=ActivityToNumber(neurons); // Neural activity to number

        if(basins[nNumber]==0)
        {
            basins[nNumber]=nattractor;
        }

        else
        {
            if(basins[nNumber]==nattractor)
            {

                if(nattractor>4000)
                {
                    printLog << "Warning: attractor > 4000";
                    exit(EXIT_FAILURE);
                }

                ++nattractor;
                while ( ( ncounter < Var._SampleSize ) && ( basins[ncounter] > 0 ) )
                {
                    ++ncounter;
                }
                if ( ncounter == Var._SampleSize )
                {
                    break;
                }

                basins[ncounter]=nattractor; //std::cout << nattractor << " ";
                neurons=NumberToActivity(ncounter); //for(i=0; i<Net._AdSize; i++){std::cout << neurons[i] << " ";}// Number to neural activity
            }

            else
            {
                for(i=0; i<Var._SampleSize; ++i)
                {
                    if(basins[i]==nattractor)
                    {
                        basins[i]=basins[nNumber];
                    }
                }

                while ( ( ncounter < Var._SampleSize ) && ( basins[ncounter] > 0 ) )
                {
                    ++ncounter;
                } //std::cout << ncounter << " ";}
                if ( ncounter == Var._SampleSize )
                {
                    break;
                }

                basins[ncounter]=nattractor; //std::cout << nattractor << " ";
                neurons=NumberToActivity(ncounter); //for(i=0; i<Net._AdSize; i++){std::cout << neurons[i] << " ";}// Number to neural activity
            }
        }
    }

    for(i=0; i<Var._SampleSize; ++i)
    {
        attractor[basins[i]][50]=attractor[basins[i]][50]+1;
    } //std::cout << attractor[basins[i]][50] << " ";}

    for(i_att=1; i_att<nattractor; ++i_att) //std::cout << i_att << " ";
    {
        ih1=0;

        while(basins[ih1]!=i_att)
        {
            ih1++;
        }
        neurons=NumberToActivity(ih1); //for(i=0; i<Net._AdSize; i++){std::cout << neurons[i] << " ";}

        //Update neural activity 70 times
        neurons=BinToBip(neurons); // NEU
        for(k=0; k<70; ++k)
        {
            for(i=0; i<Net._AdSize; i++)
            {
                helpneurons[i] = 0;
            }
            helpneurons = Update::Hebb(Matrix, neurons); // Update
            neurons = TransferFunction::StepFunctionBip(helpneurons); // Activation function
        }
        neurons=BipToBin(neurons); //for(i=0; i<Net._AdSize; i++){std::cout << neurons[i] << " ";} // NEU

        nNumber=0;

        nNumber=ActivityToNumber(neurons); // Neural activity to number

        ntest=0;
        ih2=1;
        ih3=nNumber; //std::cout << nNumber;

        while(nNumber!=ih2)  //std::cout << nNumber;
        {
            ntest++; //std::cout << ntest << " ";
            attractor[i_att][ntest]=ih3; //std::cout << attractor[i_att][ntest] << " ";

            neurons=BinToBip(neurons); //for(i=0; i<Net._AdSize; i++){std::cout << neurons[i] << " ";} // NEU
            helpneurons = {0};
            helpneurons = Update::Hebb(Matrix, neurons); // Update
            neurons = TransferFunction::StepFunctionBip(helpneurons); // Activation function
            neurons=BipToBin(neurons); //for(i=0; i<Net._AdSize; i++){std::cout << neurons[i] << " ";} // NEU

            ih3=0;
            ih3=ActivityToNumber(neurons); // Neural activity to number
            ih2=ih3; //std::cout << ih3;
        }

        attractor[i_att][51]=ntest; //std::cout << ntest << " ";
        if(ntest>50)
        {
            print << "Warning: size of attractor > 50";
            exit(EXIT_FAILURE);
        }
    }

// Print first results of the attractor analysis
    print << "\nResults of the attractor analysis:" << "\n";
    print << "\nNumber of basins: " << (nattractor-1) << "\n";

    for(i=1; i<nattractor; ++i)
    {
        print << "\nAttractor: " << i;
        print << "\nSize of the basin: " << attractor[i][50];
        print << "\nNumber of states: " << attractor[i][51] << "\n";

        for(j=1; j<(attractor[i][51]+1); ++j)
        {
            //print  << "Binary exponentiation: " << attractor[i][j];
            print  << "\nNeural Activity: ";
            ih1=attractor[i][j];
            neurons=NumberToActivity(ih1); // Number to neural activity
            neurons=BinToBip(neurons); // NEU
            for(k=0; k<Net._AdSize; ++k)
            {
                print << neurons[k] << " ";
            }
            print << "\n";
        }
    }

// Print the size of the basins
    print2Rbip_SizeOfBasins << "#Size of the basins" << "\n";
    for(i=1; i<nattractor; ++i)
    {
        print2Rbip_SizeOfBasins << attractor[i][50] << "\n";
    }

// Store attractors in a matrix (binary)
    std::vector< std::vector<int> > Attractors(nattractor-1, std::vector<int>(Net._AdSize));

    for(i=1; i<nattractor; ++i)
    {
        for(j=1; j<(attractor[i][51]+1); ++j)
        {
            ih1=attractor[i][j];
            for(k=0; k<Net._AdSize; ++k)
            {
                Attractors[i-1][k]=ih1%2;
                ih1=(ih1-Attractors[i-1][k])/2;
            }
        }
    }

// Transform binary to bipolar attractor matrix
    for(i=1; i<nattractor; ++i)
    {
        for(k=0; k<Net._AdSize; ++k)
        {
            Attractors[i-1][k]= 2*(Attractors[i-1][k])-1;
        }
    }

// Initialize configuration space
    std::vector< std::vector<int> > CSpace(Var._SampleSize, std::vector<int>(Net._AdSize +1));
    int ipattern;
    for(i=0; i<Var._SampleSize; i++)
    {
        for(j=0; j<Net._AdSize+1; j++)
        {
            CSpace[i][j] = 0;
        }
    }

// Set configuration space
    for(ipattern=0; ipattern<Var._SampleSize; ++ipattern)
    {
        j=ipattern;
        k=0;
        for (i=1; i<Net._AdSize; i++)
        {
            CSpace[ipattern][i+1]=0;
        }
        while(j>0) // Set Pattern from ipattern
        {
            CSpace[ipattern][k+1]=j%2;
            j=(j-j%2)/2;
            k=k+1;
        }
    }

// Transform binary to bipolar configuration space
    for(i=0; i<Var._SampleSize; i++)
    {
        for(auto it=CSpace[i].begin()+1; it != CSpace[i].end(); it++)
        {
            *it=2*(*it)-1;
        }
    }

// Append attractor counter to the configuration space. First Column correspond to the attractor
    std::vector<int> AttrCount(Var._SampleSize);
    for(i=0; i<Var._SampleSize; i++)
    {
        AttrCount[i]=basins[i];
    }

    for(i=0; i<Var._SampleSize; ++i)
    {
        int AttrIndex = AttrCount[i];
        CSpace[i][0]=AttrIndex;
    }

// Display Attractors
//std::cout << std::endl;
//std::cout << "\nAttractors" << "\n";
//for(const std::vector<int> &i: Attractors){
//  for(int j: i){std::cout << j << ' ';}
//std::cout << std::endl;
//}

// Display configurationspace.
//std::cout << std::endl;
//std::cout << "\n#Attractor-No.  State vector" << "\n";
//for(const std::vector<int> &i: CSpace){
//  for(int j: i){std::cout << j << ' ';}
//std::cout << std::endl;
//}

// Calculate inner products < attractors | adjoint state vectors>
// Initialize matrix DotProductAttractorsAdjointCSpace
    std::vector< std::vector<int> > DotProductAttractorsAdjointCSpace(nattractor-1, std::vector<int>(Var._SampleSize));
    std::vector< std::vector<int> > HammingDistanceAttractorsAdjointSpace(nattractor-1, std::vector<int>(Var._SampleSize));
    std::vector<int> vectorA(Net._AdSize);
    std::vector<int> vectorB(Net._AdSize);
    std::vector<int> index(nattractor-1);

    for( int y = 0; y < Var._SampleSize; y++)
    {
        for(i=0; i<Net._AdSize; ++i)
        {
            vectorA[i] = Attractors[CSpace[y][0]-1 ][i];
        }

        //std::cout << "A:  ";
        //for(auto i:vectorA){std::cout << i << " ";}
        //std::cout << std::endl;

        for( int x = 1; x < Net._AdSize+1; ++x)  //std::cout << CSpace[y][x] << "  ";
        {
            vectorB[x-1] = CSpace[y][x];
        }
        //std::cout << "nattractor=" << nattractor << std::endl;

        //std::cout << "B:  ";
        //for(auto i:vectorB){std::cout << i << " ";}
        //std::cout << std::endl;

        HammingDistanceAttractorsAdjointSpace[ CSpace[y][0] -1][index[ CSpace[y][0] -1] ] = Routine::HammingDistance(vectorA, vectorB); // Calculate Hamming distance
        DotProductAttractorsAdjointCSpace[ CSpace[y][0] -1][index[ CSpace[y][0] -1] ] = inner_product(vectorA.begin(), vectorA.end(), vectorB.begin(), 0); // Calculate inner product
        index[CSpace[y][0]-1] += 1;
    }


// Print DotProductAttractorsAdjointCSpace
    print2Rbip_DotProductAttractorsAdjointCSpace << "#DotProductAttractorsAdjointCSpace" << "\n";
    for ( const std::vector<int> &i : DotProductAttractorsAdjointCSpace )
    {
        for ( int j : i )
        {
            print2Rbip_DotProductAttractorsAdjointCSpace << j << ' ';
        }
        print2Rbip_DotProductAttractorsAdjointCSpace << "\n";
    }


// Print HammingDistanceAttractorsAdjointSpace
    print2Rbip_HammingDistanceAttractorsAdjointSpace << "#HammingDistanceAttractorsAdjointSpace" << "\n";
    for ( const std::vector<int> &i : HammingDistanceAttractorsAdjointSpace )
    {
        for ( int j : i )
        {
            print2Rbip_HammingDistanceAttractorsAdjointSpace << j << ' ';
        }
        print2Rbip_HammingDistanceAttractorsAdjointSpace << "\n";
    }

    /*
    // _____________________________________________________________________
    // C-attractor output
    std::ofstream printCattractor("./CattractorOutput.txt",std::ios_base::app);
    printCattractor << "\n=====C-Attractor output==================Bipolar encoding=============" << std::endl;

    int neuronen_bip[Net._AdSize];
    int hilfsneuronen_bip[Net._AdSize];
    int hilfsbild[Net._AdSize];
    int ik, il, in, ip, ibild, itest, mtest, ncount;
    int ncorrectimage;               // counts correct recognized pattern

    // Algorithm
    ncorrectimage=0;
    for(ibild=0;ibild<Var._SampleSize;++ibild){

       il=ibild; in=0;

       for(ik=0;ik<Net._AdSize;++ik){ // Initialisierung des Testmusters
         hilfsbild[ik]=0;
       }

       while(il>0){ // Bestimmung des Testmusters aus ibild
         hilfsbild[in]=il%2;
         il=(il-il%2)/2;
         in=in+1;
       }

       printCattractor << "\nPattern " << ibild << ": ";

       for(ik=0;ik<Net._AdSize;++ik){
         printCattractor << 2*hilfsbild[ik]-1 << " "; //NEU
       }

       printCattractor << "    ";

       for(ik=0;ik<Net._AdSize;++ik) {
         neuronen_bip[ik]=2*hilfsbild[ik]-1;
       }

       itest=1;
       ntest=0;
       ncount=1;
       mtest=0;

       while (itest>0 && mtest<50) {

          for(ik=0;ik<Net._AdSize;++ik) {

            hilfsneuronen_bip[ik]=0;

            for(il=0;il<Net._AdSize;++il) {
              hilfsneuronen_bip[ik]=hilfsneuronen_bip[ik]+Matrix[ik][il]*neuronen_bip[il]; //Update Regel
            }

            if(hilfsneuronen_bip[ik]>=Theta._Threshold) { //Treshold
              hilfsneuronen_bip[ik]=1;
            }

            else hilfsneuronen_bip[ik]=-1;
            in=((neuronen_bip[ik]+1)/2-(hilfsbild[ik]+1)/2);//NEU
            ntest=ntest+in*in; //std::cout << "\nntest: " << ntest << " in: " << in;
          }

          if(mtest>45){
             printCattractor << " mtest: " << mtest << " Activity:  ";

             for(ip=0;ip<Net._AdSize;++ip) {
               printCattractor << neuronen_bip[ip] << " "; printCattractor << neuronen_bip[ip] << " ";
             }

             printCattractor << "\n";
          }

          itest=ntest;
          mtest=mtest+1;
          ntest=0;
          ncount=ncount+1;

          for (ik=0;ik<Net._AdSize;++ik) {neuronen_bip[ik]=hilfsneuronen_bip[ik];}
       }

       printCattractor << "   Attractor: "; //std::cout << "\nn= " << ncount;

       for (ik=0;ik<Net._AdSize;++ik){
         printCattractor << neuronen_bip[ik] << " ";
       }

       il=0;

    //   for (ik=0;ik<Net._AdSize;++ik){
    //     in=(neuronen_bip[ik]-hilfsbild[ik]);
    //     il=il+in*in; //std::cout << "\nil: " << il << " in: " << in;
    //   }

       for (ik=0;ik<Net._AdSize;++ik){
         in=((neuronen_bip[ik]+1)/2-(hilfsbild[ik]+1)/2); //NEU
         il=il+in*in; //std::cout << "\nil: " << il << " in: " << in;
       }


       if (il<1){
         ncorrectimage=ncorrectimage+1;
         printCattractor << std::endl << "Attractor No.: " << ncorrectimage << std::endl;
       }

    }
    printCattractor << std::endl << "\nNumber of found attractors: " << ncorrectimage << std::endl;
    // _____________________________________________________________________
    */

    return Attractors;
}
// _____________________________________________________________________
