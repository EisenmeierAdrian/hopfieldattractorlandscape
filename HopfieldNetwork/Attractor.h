//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#ifndef ATTRACTOR_H
#define ATTRACTOR_H

#include <vector>

// A class for the attractor analysis
class Attractor
{
public:
    Attractor(); //Constructor

    static int ActivityToNumber(std::vector<int> & Activity); // Convert a binary neural activity to a number

    static std::vector<int> NumberToActivity(int & number); // Convert a number to a binary neural activity

    static std::vector<int> BinToBip(std::vector<int> & Activity); // Convert binary activity to a bipolar activity

    static std::vector<int> BipToBin(std::vector<int> & Activity); // Convert bipolar activity to a binary activity

    static std::vector<std::vector<int> > AnalysisBinary(std::vector< std::vector<int> > & Matrix); // Analysis of the attractor landscape in binary encoding

    static std::vector<std::vector<int> > AnalysisBipolar(std::vector< std::vector<int> > & Matrix); // Analysis of the attractor landscape in bipolar encoding
};

#endif  // ATTRACTOR

