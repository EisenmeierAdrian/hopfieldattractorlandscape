//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#include <time.h>
#include <fstream>
#include <iostream>
#include <iomanip>      // std::setw
#include "Routine.h"
#include "Pattern.h"
#include "Network.h"
#include "Update.h"
#include "TransferFunction.h"
#include "Attractor.h"

using namespace std;

int main(int argc, char** argv)
{

    clock_t tStart = clock();   // Initialize clock for run time measurement
    ofstream print("./Output.txt",ios_base::app);  // Printing in a text file

    Network network;            
    Pattern trained;            
    TransferFunction value;     

// General information
    print << "Hopfield Network" << endl << "Copyright 2016, University of Freiburg" << "\nAuthor: Adrian Eisenmeier <eisenmeieradrian@yahoo.de>" << endl;
    print << "\nNeurons: " << network._AdSize << endl;
    print << "Stored pattern: " << trained._MaxPattern  << endl;
    print << "Transfer function: step function with threshold: " << "binary: " << value._ThresholdBin << " bipolar: " << value._ThresholdBip << endl;
    print << std::endl << "==================Binary encoding==================" << std::endl;

// Initialize stored binary pattern
    vector< vector<int> > pattern(trained._MaxPattern, vector<int>(network._AdSize));

    /*
    // In case I want to set the stored pattern by hand
    pattern =    {{1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1},
                  {0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0},
                  {0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0}};
    */

//pattern = Pattern::Random();           // Create totally random binary pattern
//pattern = Pattern::Orthonormal();      // Create orthonormal pattern
    pattern = Pattern::Sparse();       // Create sparse binary pattern

// Print stored pattern
    print << "\nStored pattern: " << endl;
    for(const vector<int> &i: pattern)
    {
        for(int j: i)
        {
            print << setw(2) << j << "  ";
        }
        print << endl;
    }

// Calculate adjacency matrix (with self-connection)
    vector< vector<int> > adjacency(network._AdSize, vector<int>(network._AdSize));
    adjacency = Network::AdjacencySelfConnection(pattern);

// Set diagonal elements to zero (without self-connection)
//adjacency = Network::AdjacencyNoSelfConnection(adjacency);

// Print adjacency matrix
    print << "\nAdjacency matrix:" << endl;
    for(const vector<int> &i: adjacency)
    {
        for(int j: i)
        {
            print << setw(2) << j << "  ";
        }
        print << endl;
    }

// Attractor analysis
    vector<vector<int> > attractors;
    attractors = Attractor::AnalysisBinary(adjacency);

    Routine::MatricesRowComparison(pattern, attractors);

// _____________________________________________________________________

    print << std::endl;
    print << std::endl << "==================Bipolar encoding==================" << std::endl;

// Convert the binary {0,1} to a bipolar {-1,1} pattern
    pattern=Pattern::ConvertBinaryToBipolar(pattern);

// Print stored pattern
    print << "\nStored pattern: " << endl;
    for(const vector<int> &i: pattern)
    {
        for(int j: i)
        {
            print << setw(2) << j << "  ";
        }
        print << endl;
    }

// Calculate adjacency matrix (with self-connection)
    adjacency = Network::AdjacencySelfConnection(pattern);

// Set diagonal elements to zero (without self-connection)
//adjacency = Network::AdjacencyNoSelfConnection(adjacency);

// Print adjacency matrix
    print << "\nAdjacency matrix:" << endl;
    for(const vector<int> &i: adjacency)
    {
        for(int j: i)
        {
            print << setw(2) << j << "  ";
        }
        print << endl;
    }

    attractors = Attractor::AnalysisBipolar(adjacency);

    Routine::MatricesRowComparison(pattern, attractors);

// _____________________________________________________________________

// Statistical evaluation of the attractor analysis
    system("/bin/bash -c 'Rscript StatisticalAnalysis.R' ");

    print << std::endl << "\nRuntime in sec. " << (double)(clock()-tStart)/CLOCKS_PER_SEC << endl; // Calculate run time
    return 0;                   // Exit code for end of the main-function
}
