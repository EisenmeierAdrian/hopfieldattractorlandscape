//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#include <cmath>
#include <vector>
#include "Network.h"
#include "Pattern.h"

#include <iostream>
// _____________________________________________________________________
// Constructor for the Network class.
Network::Network() : _AdSize(4)   // Number of Neurons
{

    _SampleSize=std::pow(2,_AdSize);
}
// _____________________________________________________________________
// Implementation of Adjacency matrix with self-connection
std::vector< std::vector<int> > Network::AdjacencySelfConnection(std::vector< std::vector<int> >  & Pictures)
{
    Network N;
    Pattern l;

// Initialize adjacency matrix
    std::vector< std::vector<int> > Matrix(N._AdSize, std::vector<int>(N._AdSize));

// Set adjacency matrix
    for(int ik=0; ik<N._AdSize; ++ik)
    {
        for(int il=0; il<N._AdSize; ++il)
        {
            for(int in=0; in<l._MaxPattern; ++in)
            {
                Matrix[ik][il]=Matrix[ik][il] + Pictures[in][ik]*Pictures[in][il] ;
            }
        }
    }

    return Matrix;
}
// _____________________________________________________________________
// Implementation of Adjacency matrix without self-connection
std::vector< std::vector<int> > Network::AdjacencyNoSelfConnection(std::vector< std::vector<int> > & Pictures)
{
    Network N;

// Set diagonal elements to zero
    for(int ik=0; ik<N._AdSize; ++ik)
    {
        Pictures[ik][ik]=0 ;
    }
    return Pictures;
}
// _____________________________________________________________________
