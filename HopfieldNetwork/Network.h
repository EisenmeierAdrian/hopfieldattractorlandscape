//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#ifndef NETWORK_H
#define NETWORK_H

#include <cstdlib>
#include <vector>

// A class for the network topology
class Network
{
public:
    Network(); //Constructor

    int const _AdSize; // Number of Neurons
    int _SampleSize; // Number of Rows in configuration space

    static std::vector< std::vector<int> > AdjacencySelfConnection(std::vector< std::vector<int> > & Pictures);   // Create adjacency matrix with self connection
    static std::vector< std::vector<int> > AdjacencyNoSelfConnection(std::vector< std::vector<int> > & Pictures); // Create adjacency matrix without self connection
};

#endif  // NETWORK_H
