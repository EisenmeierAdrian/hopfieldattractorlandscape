//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#include <cstdlib>
#include <vector>
#include <numeric>
#include <sstream>
#include <fstream>
#include "Pattern.h"
#include "Network.h"
#include "Routine.h"

// _____________________________________________________________________
// Constructor for the Pattern class.
Pattern::Pattern() : _MaxPattern(2)   // Number of learned pictures
{

    _NumOnes = 2;   // Control number of 1's in a row for the orthogonal pattern:
}
// _____________________________________________________________________
// Convert the binary {0,1} to a bipolar {-1,1} pattern
std::vector< std::vector<int> > Pattern::ConvertBinaryToBipolar(std::vector< std::vector<int> > & Matrix)
{
    Network net;
    Pattern L;
    std::vector< std::vector<int> > PatternBipolar(L._MaxPattern, std::vector<int>(net._AdSize));

    for(int i=0; i<L._MaxPattern; i++)
    {
        for(int j=0; j<net._AdSize; j++)
        {
            PatternBipolar[i][j]=2*Matrix[i][j]-1;
        }
    }

    return PatternBipolar;
}
// _____________________________________________________________________
// Create totally random binary pattern
std::vector< std::vector<int> > Pattern::Random()
{
    Network net;
    Pattern L;
    std::vector< std::vector<int> > random(L._MaxPattern, std::vector<int>(net._AdSize));

    for(int i=0; i<L._MaxPattern; i++)
    {
        for(int j=0; j<net._AdSize; j++)
        {
            random[i][j]=std::rand()%2;
        }
    }

    return random;
}
// _____________________________________________________________________
// Create sparse binary pattern
std::vector< std::vector<int> > Pattern::Sparse()
{
// Initialize random sequence
    Network net;
    std::vector<int> sequence (net._AdSize);
    for(int i=0; i<net._AdSize; i++)
    {
        sequence[i]=0;
    }
    std::iota(sequence.begin(), sequence.end(), 0); // Fill sequence
    Routine::FisherYates(sequence);  // Shuffle the sequence

// Initialize matrix for the learned pattern
    Pattern L;
    std::vector< std::vector<int> > orthogonal(L._MaxPattern, std::vector<int>(net._AdSize));

    std::stringstream sstr;
    int z;
    z = net._AdSize/L._MaxPattern;
    Pattern I;
    int ik, il;
    ik=0;
    il=0;

// Set orthogonal pattern
    for(ik=0; ik < L._MaxPattern; ik++)
    {
        for(sstr.str()[il]; il < z; il++)
        {
            orthogonal[ik][sequence[il]] = 1;
        }
        z = z + I._NumOnes;
    }

    return orthogonal;
}
// _____________________________________________________________________
// Create orthonormal binary pattern
std::vector< std::vector<int> > Pattern::Orthonormal()
{
// Initialize random sequence
    Network net;
    std::vector<int> sequence (net._AdSize);
    for(int i=0; i<net._AdSize; i++)
    {
        sequence[i]=0;
    }
    std::iota(sequence.begin(), sequence.end(), 0); // Fill sequence
    Routine::FisherYates(sequence);  // Shuffle the sequence

// Initialize matrix for the learned pattern
    Pattern L;
    std::vector< std::vector<int> > orthonormal(L._MaxPattern, std::vector<int>(net._AdSize));
    int j;
    j=0;
    for(int i=0; i<L._MaxPattern; i++)
    {
        orthonormal[i][sequence[j]]=1;
        j++;
    }

    return orthonormal;
}
// _____________________________________________________________________
// Create a binary test pattern
std::vector<int > Pattern::Test()
{
    Network net;
    std::vector<int> TestPattern(net._AdSize);

    for(int j=0; j<net._AdSize; j++)
    {
        TestPattern[j]=std::rand()%2;
    }

    return TestPattern;
}
// _____________________________________________________________________
