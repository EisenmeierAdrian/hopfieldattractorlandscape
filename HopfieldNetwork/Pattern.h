//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#ifndef PATTERN_H
#define PATTERN_H

#include <vector>

// A class for the creation of various pattern
class Pattern
{
public:
    // Constructor
    Pattern();

    // Max. number of pattern
    int const _MaxPattern;

    // Create orthonormal pattern
    static std::vector< std::vector<int> > Orthonormal();

    // Create sparse binary pattern
    static std::vector< std::vector<int> > Sparse();

    // Create totally random binary patten
    static std::vector< std::vector<int> > Random();

    // Create binary test pattern
    static std::vector<int> Test();

    // Convert the binary {0,1} to a bipolar {-1,1} pattern
    static std::vector< std::vector<int> > ConvertBinaryToBipolar(std::vector< std::vector<int> > & Matrix);

private:
    int _NumOnes; // Control number of 1's in a row for the orthogonal pattern:

};

#endif  // PATTERN_H
