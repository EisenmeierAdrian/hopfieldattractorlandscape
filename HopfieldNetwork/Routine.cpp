//  Copyright 2016, University of Freiburg.
//  Author: Adrian Eisenmeier <eisenmeieradrian@yahoo.de>.

#include <vector>
#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "./Routine.h"


// _____________________________________________________________________
// Fisher-Yates shuffle algorithm of a vector
void Routine::FisherYates(std::vector<int> &vec)
{
    int n = vec.size();
    for(int i=n-1; i>0; --i)
    {
        std::swap(vec[i], vec[std::rand() % (i+1)]);
    }
}
// _____________________________________________________________________
// Hamming distance of two vectors
int Routine::HammingDistance(std::vector<int> &vecA, std::vector<int> &vecB)
{
    int hm_distance;
    hm_distance=0;

    for(size_t i=0; i<vecA.size(); i++)
    {
        if(!(vecA[i]==vecB[i]))
        {
            hm_distance++;
        }
        else
        {
            hm_distance=hm_distance+0;
        }
    }

    return hm_distance;
}
// _____________________________________________________________________
// Compare the Hamming distance for each possible row of two matrices
void Routine::MatricesRowComparison(std::vector< std::vector<int> > &MatrixA, std::vector< std::vector<int> > &MatrixB)
{
    std::ofstream print("./Output.txt",std::ios_base::app);

    int rowsA, columnsA, rowsB, dist;
    rowsA=MatrixA.size();
    columnsA=MatrixA[1].size();
    rowsB=MatrixB.size();

    std::vector<int> vectorsA(columnsA);
    std::vector<int> vectorsB(columnsA);

    for(int r=0; r<rowsA; ++r)
    {
        for(int s=0; s<rowsB; ++s)
        {
            for(int n=0; n<columnsA; ++n)
            {
                vectorsA[n]=MatrixA[r][n];
                vectorsB[n]=MatrixB[s][n];
            }
            dist = Routine::HammingDistance(vectorsA, vectorsB); // Calculate Hamming distance
            if(dist==0)
            {
                print << std::endl << "Attractor " << s+1 << " is fixpoint for pattern " << r+1;
            }
            else
            {
                continue;
            }
        }
    }


}
// _____________________________________________________________________
