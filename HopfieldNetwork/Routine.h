//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#include <vector>

// A namespace for routines and algorithms
namespace Routine
{

// Fisher-Yates shuffle algorithm of a vector
void FisherYates(std::vector<int> &vec);

// Hamming distance (bitwise)
int HammingDistance(std::vector<int> &vecA, std::vector<int> &vecB);

// Compare the Hamming distance for each possible row of two matrices (same number of columns necessary)
void MatricesRowComparison(std::vector< std::vector<int> > &MatrixA, std::vector< std::vector<int> > &MatrixB);
}
