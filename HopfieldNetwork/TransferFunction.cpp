//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#include <vector>
#include "TransferFunction.h"
#include "Network.h"

#include <iostream>
// _____________________________________________________________________
// Constructor for the TransferFunction class.
TransferFunction::TransferFunction() : _ThresholdBin(1.5), _ThresholdBip(1.5)   // Threshold value for the activation
{

}
// _____________________________________________________________________
// Define step-function for binary encoding
std::vector<int> TransferFunction::StepFunctionBin(std::vector<int> & vec)
{
    TransferFunction Theta;
    Network J;

    for(int j=0; j < J._AdSize; j++)
    {
        if(vec[j] > Theta._ThresholdBin)
        {
            vec[j] = 1;
        }
        else
        {
            vec[j] = 0;
        }
    }

    return vec;
}
// _____________________________________________________________________
// Define step-function for bipolar encoding
std::vector<int> TransferFunction::StepFunctionBip(std::vector<int> & vec)
{
    TransferFunction Theta;
    Network J;

    for(int j=0; j < J._AdSize; j++)
    {
        if(vec[j] > Theta._ThresholdBip)
        {
            vec[j] = 1;
        }
        else
        {
            vec[j] = -1;
        }
    }

    return vec;
}
// _____________________________________________________________________
