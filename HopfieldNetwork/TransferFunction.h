//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#ifndef TRANSFERFUNCTION_H
#define TRANSFERFUNCTION_H

#include <vector>

// A class for the various transferfunctions
class TransferFunction
{
public:
    TransferFunction(); //Constructor

    float const _ThresholdBin;     // Threshold value for the Heavyside function for binary encoding

    float const _ThresholdBip;     // Threshold value for the Heavyside function for bipolar encoding

    static std::vector<int> StepFunctionBin(std::vector<int> & vec); // Declare step-function for binary encoding

    static std::vector<int> StepFunctionBip(std::vector<int> & vec); // Declare step-function for bipolar encoding
};

#endif  // TRANSFERFUNCTION
