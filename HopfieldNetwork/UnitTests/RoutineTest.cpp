//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#include <gtest/gtest.h>
#include "../Routine.cpp"

// _____________________________________________________________________
// Unit-Test of the Hamming-Distance Function
std::vector<int> a = {1,0,1,0}; std::vector<int> b = {1,0,1,0};
std::vector<int> c = {1,1,1,0}; std::vector<int> d = {0,0,0,1};

TEST(Routine, HammingDistance) {
  ASSERT_EQ(0, Routine::HammingDistance(a,b));
  ASSERT_EQ(4, Routine::HammingDistance(c,d));
}

// _____________________________________________________________________
int main(int argc, char** argv) {
 ::testing::InitGoogleTest(&argc, argv);
 return RUN_ALL_TESTS();
}
