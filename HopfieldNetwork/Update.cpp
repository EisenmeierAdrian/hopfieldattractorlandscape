//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#include <vector>
#include "Update.h"
#include "Network.h"

#include <iostream>
// _____________________________________________________________________
// Constructor for the Update class.
Update::Update()
{

}
// _____________________________________________________________________
// Define Hebbian learning
std::vector<int> Update::Hebb(std::vector< std::vector<int> > & Adjacency, std::vector<int> & Pattern)
{
    Network Net;
    std::vector<int> TmpPattern(Net._AdSize);

    for(int i=0; i<Net._AdSize; ++i)
    {
        TmpPattern[i] = 0;
        for(int j=0; j<Net._AdSize; ++j)
        {
            TmpPattern[i] = TmpPattern[i] + Adjacency[i][j]*Pattern[j];
        }
    }

    return TmpPattern;
}
// _____________________________________________________________________
