//  Author: Adrian Eisenmeier <adrian.eisenmeier@it-security-eisenmeier.de>.

#ifndef UPDATE_H
#define UPDATE_H

#include <vector>

// A class for the update rules
class Update
{
public:
    Update(); //Constructor

    static std::vector<int> Hebb(std::vector< std::vector<int> > & Adjacency, std::vector<int> & Pattern); // Use Hebbian learning
};

#endif  // UPDATE_H
