Stuff to do:

1. Remove duplicate code of binary and bipolar encoding in the attractor class. Don't repeat yourself

2. multithreading for the search of fix points in the attractor landscape

3. using templates for integer and float encoding

4. implementing of asynchronous Hebbian update rule

5. implementing of statistical sampling for working with more than about 20 neurons

6. implementing different activation functions

7. reprogramming of the statistical analysis R script

8. interface for linear algebra operations on the adjacence matrix of the network graph